delete from Ingredient_Ref;
delete from Shaurma;
delete from Shaurma_Order;

delete from Ingredient;
insert into Ingredient (id, name, type)
                        values ('CHS', 'Cheese pita','PITA');
insert into Ingredient (id, name, type)
                        values ('TMT', 'Tomato pita','PITA');
insert into Ingredient (id, name, type)
                        values ('GRL', 'Garlic pita','PITA');
insert into Ingredient (id, name, type)
                        values ('CHC', 'Chicken','MEAT');
insert into Ingredient (id, name, type)
                        values ('PRK', 'Pork','MEAT');
insert into Ingredient (id, name, type)
                        values ('VGT', 'Vegetable mix','VEGGIES');
insert into Ingredient (id, name, type)
                        values ('CCM', 'Cucumber','VEGGIES');
insert into Ingredient (id, name, type)
                        values ('SPC', 'Spicy sauce','SAUCE');
insert into Ingredient (id, name, type)
                        values ('CHSS', 'Cheese sauce','SAUCE');