package com.shaurma.shaurma.repository;

import com.shaurma.shaurma.models.ShaurmaOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<ShaurmaOrder, Long> {
    List<ShaurmaOrder> findByDeliveryZip(String deliveryZip);
}
