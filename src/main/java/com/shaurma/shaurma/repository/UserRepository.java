package com.shaurma.shaurma.repository;

import com.shaurma.shaurma.models.ShaurmaUser;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<ShaurmaUser, Long> {
    ShaurmaUser findShaurmaUserByUsername(String username);
}
