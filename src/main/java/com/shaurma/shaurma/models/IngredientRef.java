package com.shaurma.shaurma.models;

import lombok.Data;

@Data
public class IngredientRef {

    private final String ingredient;
}
