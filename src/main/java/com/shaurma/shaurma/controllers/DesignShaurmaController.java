package com.shaurma.shaurma.controllers;

import com.shaurma.shaurma.models.Ingredient;
import com.shaurma.shaurma.models.Shaurma;
import com.shaurma.shaurma.repository.IngredientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@SessionAttributes("shaurmaOrder")
@RequestMapping("/design")
public class DesignShaurmaController {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public DesignShaurmaController(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @ModelAttribute
    public void addIngredientsToModel(Model model) {
        Iterable<Ingredient> ingredients = ingredientRepository.findAll();
        Ingredient.Type[] types = Ingredient.Type.values();
        for (Ingredient.Type type : types) {
            model.addAttribute(type.toString().toLowerCase(), filterByType((List<Ingredient>) ingredients, type));
        }
    }

    @GetMapping
    public String showDesignForm(Model model) {
        model.addAttribute("shaurma", new Shaurma());
        return "design";
    }

    private Iterable<Ingredient> filterByType(List<Ingredient> ingredients, Ingredient.Type type) {
        return ingredients
                .stream()
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }

    @PostMapping
    public String processShaurma(@Valid @ModelAttribute("shaurma") Shaurma shaurma, Errors errors) {
        if (errors.hasErrors()) {
            return "design";
        }
        log.info("Processing shaurma: " + shaurma);
        return "redirect:/orders/current";
    }
}
